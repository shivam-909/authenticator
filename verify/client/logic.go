package client

import "context"

// logic contains all business logic methods for this package to expose.

type Client interface {
	VerifyPhoneNumber(ctx context.Context, pn, otp string) error
	VerifyEmailAddress(ctx context.Context, pn, otp string) error
	RequestNewOTP(ctx context.Context, pn string) error
	RequestNewEmailCode(ctx context.Context, pn string) error
}

type client struct{}

func NewLogicalClient() Client {
	return &client{}
}

func (client) VerifyPhoneNumber(ctx context.Context, pn, otp string) error {
	panic("not implemented")
}

func (client) VerifyEmailAddress(ctx context.Context, pn, otp string) error {
	panic("not implemented")
}

func (client) RequestNewOTP(ctx context.Context, pn string) error {
	panic("not implemented")
}

func (client) RequestNewEmailCode(ctx context.Context, pn string) error {
	panic("not implemented")
}
