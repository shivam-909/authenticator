module gitlab.com/shivam-909/authenticator

go 1.18

require (
	github.com/bufbuild/connect-go v0.3.0
	github.com/gocql/gocql v1.2.0
	github.com/golang-jwt/jwt/v4 v4.4.2
	gitlab.com/shivam-909/heck v0.0.0-20220813143030-7478a155fc4b
	gitlab.com/shivam-909/hermes v0.0.0-20220807150450-24ad637d01b3
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/cassini-engineering/wtf v0.0.0-20220717174648-a3af3ceffcd1 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/klauspost/compress v1.15.9 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.38.0 // indirect
	golang.org/x/net v0.0.0-20220805013720-a33c5aa5df48 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
)
