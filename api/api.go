package api

import (
	"context"
	"fmt"
	"net/http"

	"github.com/bufbuild/connect-go"
	"gitlab.com/shivam-909/authenticator/authority/client/authoritypb"
	"gitlab.com/shivam-909/authenticator/authority/client/authoritypb/authoritypbconnect"
	"gitlab.com/shivam-909/authenticator/users/client/userspb"
	"gitlab.com/shivam-909/authenticator/users/client/userspb/userspbconnect"
	"gitlab.com/shivam-909/authenticator/verify/client/verifypb"
	"gitlab.com/shivam-909/authenticator/verify/client/verifypb/verifypbconnect"
)

const (
	ServiceName = "authenticator"
)

type Client interface {
	// -------------------- AUTHORITY --------------------

	// INTERNAL USE ONLY
	// Hello is a health check endpoint.
	Hello(ctx context.Context, req *connect.Request[authoritypb.Empty]) (*connect.Response[authoritypb.HelloResponse], error)

	// PUBLIC
	// Register User initiates the registration process and creates a new user entity.
	RegisterUser(ctx context.Context, req *connect.Request[authoritypb.RegisterUserRequest]) (*connect.Response[authoritypb.RegisterUserResponse], error)

	// PUBLIC
	// Authenticate takes an access token in and makes sure it is valid, secure and matches to a known user.
	Authenticate(ctx context.Context, req *connect.Request[authoritypb.AuthenticateRequest]) (*connect.Response[authoritypb.AuthenticateResponse], error)

	// PUBLIC
	// RefreshToken refreshes a token pair.
	RefreshToken(ctx context.Context, req *connect.Request[authoritypb.RefreshTokenRequest]) (*connect.Response[authoritypb.RefreshTokenResponse], error)

	// -------------------- USERS --------------------

	// INTERNAL
	LookupUser(ctx context.Context, req *connect.Request[userspb.IdRequest]) (*connect.Response[userspb.User], error)

	// PUBLIC
	UpdateUser(ctx context.Context, req *connect.Request[userspb.UpdateUserRequest]) (*connect.Response[userspb.Empty], error)

	// PUBLIC
	DeleteUser(ctx context.Context, req *connect.Request[userspb.IdRequest]) (*connect.Response[userspb.Empty], error)

	// -------------------- VERIFY --------------------

	// PUBLIC
	VerifyPhoneNumber(ctx context.Context, req *connect.Request[verifypb.VerifyContactRequest]) (*connect.Response[verifypb.Empty], error)

	// PUBLIC
	RequestNewOTP(ctx context.Context, req *connect.Request[verifypb.Empty]) (*connect.Response[verifypb.Empty], error)

	// PUBLIC
	ResendEmailCode(ctx context.Context, req *connect.Request[verifypb.Empty]) (*connect.Response[verifypb.Empty], error)

	// PUBLIC
	VerifyEmailAddress(ctx context.Context, req *connect.Request[verifypb.VerifyContactRequest]) (*connect.Response[verifypb.Empty], error)
}

type c struct {
	authority_client authoritypbconnect.AuthorityServiceClient
	users_client     userspbconnect.UsersServiceClient
	verify_client    verifypbconnect.VerifyServiceClient
}

func NewClient(port int32, local bool) Client {

	addr := fmt.Sprintf("http://%v-svc:%d", ServiceName, port)
	if local {
		addr = fmt.Sprintf("http://localhost:%d", port)
	}

	authcl := authoritypbconnect.NewAuthorityServiceClient(http.DefaultClient, addr)
	userscl := userspbconnect.NewUsersServiceClient(http.DefaultClient, addr)
	verifycl := verifypbconnect.NewVerifyServiceClient(http.DefaultClient, addr)

	cl := &c{
		authority_client: authcl,
		users_client:     userscl,
		verify_client:    verifycl,
	}

	return cl
}

// -------------------- AUTHORITY --------------------
func (cl *c) Hello(ctx context.Context, req *connect.Request[authoritypb.Empty]) (*connect.Response[authoritypb.HelloResponse], error) {
	return cl.authority_client.Hello(ctx, req)
}

func (cl *c) RegisterUser(ctx context.Context, req *connect.Request[authoritypb.RegisterUserRequest]) (*connect.Response[authoritypb.RegisterUserResponse], error) {
	return cl.authority_client.RegisterUser(ctx, req)
}

func (cl *c) Authenticate(ctx context.Context, req *connect.Request[authoritypb.AuthenticateRequest]) (*connect.Response[authoritypb.AuthenticateResponse], error) {
	return cl.authority_client.Authenticate(ctx, req)
}

func (cl *c) RefreshToken(ctx context.Context, req *connect.Request[authoritypb.RefreshTokenRequest]) (*connect.Response[authoritypb.RefreshTokenResponse], error) {
	return cl.authority_client.RefreshToken(ctx, req)
}

// -------------------- USERS --------------------
func (cl *c) LookupUser(ctx context.Context, req *connect.Request[userspb.IdRequest]) (*connect.Response[userspb.User], error) {
	return cl.users_client.Lookup(ctx, req)
}

func (cl *c) UpdateUser(ctx context.Context, req *connect.Request[userspb.UpdateUserRequest]) (*connect.Response[userspb.Empty], error) {
	return cl.users_client.UpdateUser(ctx, req)
}

func (cl *c) DeleteUser(ctx context.Context, req *connect.Request[userspb.IdRequest]) (*connect.Response[userspb.Empty], error) {
	return cl.users_client.DeleteUser(ctx, req)
}

// -------------------- VERIFY --------------------
func (cl *c) VerifyPhoneNumber(ctx context.Context, req *connect.Request[verifypb.VerifyContactRequest]) (*connect.Response[verifypb.Empty], error) {
	return cl.verify_client.VerifyPhoneNumber(ctx, req)
}
func (cl *c) RequestNewOTP(ctx context.Context, req *connect.Request[verifypb.Empty]) (*connect.Response[verifypb.Empty], error) {
	return cl.verify_client.RequestNewOTP(ctx, req)
}
func (cl *c) ResendEmailCode(ctx context.Context, req *connect.Request[verifypb.Empty]) (*connect.Response[verifypb.Empty], error) {
	return cl.verify_client.ResendEmailCode(ctx, req)
}
func (cl *c) VerifyEmailAddress(ctx context.Context, req *connect.Request[verifypb.VerifyContactRequest]) (*connect.Response[verifypb.Empty], error) {
	return cl.verify_client.VerifyEmailAddress(ctx, req)
}
