package db

import (
	"context"
	"log"
	"time"

	"github.com/gocql/gocql"
	"gitlab.com/shivam-909/authenticator/public"
)

const (
	allFields = " created_at, updated_at, email_address, phone_number, verified, device_identifier, pin_hash "

	insertUser = "INSERT INTO authenticator.users ( created_at, updated_at, phone_number, verified, device_identifier) VALUES (?, ?, ?, ?, ?);"

	lookupUser = "SELECT" + allFields + "from authenticator.users WHERE phone_number = ? LIMIT 1;"

	deleteUser = "DELETE FROM authenticator.users WHERE phone_number = ?;"

	updateUser = `
	UPDATE authenticator.users
	SET phone_number = IsNull(@phone_number,  ?), 
	email_address = IsNull(@email_address,  ?),
	device_identifier = IsNull(@device_identifier,  ?),
	pin_hash = IsNull(@pin_hash,  ?)
	WHERE phone_number = ?; 
	`
)

func CreateUser(ctx context.Context, dbc *gocql.Session, phoneNumber, deviceIdentifier string) (*public.User, error) {

	err := dbc.Query(insertUser, time.Now(), time.Now(), phoneNumber, false, deviceIdentifier).WithContext(ctx).Exec()
	if err != nil {
		log.Printf("failed to insert new user: %v", err)
		return nil, err
	}

	user, err := scanUser(dbc.Query(lookupUser, phoneNumber))
	if err != nil {
		return nil, err
	}

	return user, nil
}

func LookupUser(ctx context.Context, dbc *gocql.Session, pn string) (*public.User, error) {
	user, err := scanUser(dbc.Query(lookupUser, pn))
	if err != nil {
		return nil, err
	}
	return user, err
}

func UpdateUser(ctx context.Context, dbc *gocql.Session, u *public.User) error {
	var phoneNumber, emailAddress, deviceIdentifer, pinHash interface{}

	if u.PhoneNumber == "" {
		phoneNumber = nil
	} else {
		phoneNumber = u.PhoneNumber
	}

	if u.EmailAddress == "" {
		emailAddress = nil
	} else {
		emailAddress = u.EmailAddress
	}

	if u.DeviceIdentifier == "" {
		deviceIdentifer = nil
	} else {
		deviceIdentifer = u.DeviceIdentifier
	}

	if u.PinHash == "" {
		pinHash = nil
	} else {
		pinHash = u.PinHash
	}

	err := dbc.Query(updateUser, phoneNumber, emailAddress, deviceIdentifer, pinHash).Exec()
	if err != nil {
		return err
	}
	return nil
}

func DeleteUser(ctx context.Context, dbc *gocql.Session, phoneNumber string) error {
	return dbc.Query(deleteUser, phoneNumber).Exec()
}

func scanUser(q *gocql.Query) (*public.User, error) {
	user := &public.User{}
	err := q.Scan(&user.CreatedAt, &user.UpdatedAt, &user.EmailAddress, &user.PhoneNumber, &user.Verified, &user.DeviceIdentifier, &user.PinHash)
	if err != nil {
		return nil, err
	}
	return user, nil
}
