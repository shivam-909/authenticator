package users

import (
	"context"

	"gitlab.com/shivam-909/authenticator/users/db"
	"gitlab.com/shivam-909/heck"
)

func CreateUser(ctx context.Context, b Backends, pn, di string) error {
	_, err := db.LookupUser(ctx, b.Cassandra(), pn)
	if err == nil {
		return nil
	}

	_, err = db.CreateUser(ctx, b.Cassandra(), pn, di)
	if err != nil {
		return heck.Wrap("failed to create a new user", err)
	}

	return nil
}
