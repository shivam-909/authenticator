package users

import "github.com/gocql/gocql"

type Backends interface {
	Cassandra() *gocql.Session
}
