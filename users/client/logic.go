// logic contains all business for this package to expose.
package client

import (
	"context"
	"errors"

	"gitlab.com/shivam-909/authenticator/public"
	"gitlab.com/shivam-909/authenticator/users"
	"gitlab.com/shivam-909/authenticator/users/client/userspb"
	"gitlab.com/shivam-909/authenticator/users/db"
)

type Client interface {
	CreateUser(ctx context.Context, b users.Backends, phoneNumber, deviceIdentifier string) error
	LookupUser(ctx context.Context, b users.Backends, phoneNumber string) (*public.User, error)
	UpdateUser(ctx context.Context, b users.Backends, pn string, u *userspb.User) error
	DeleteUser(ctx context.Context, b users.Backends, pn string) error
}

type client struct{}

func NewLogicalClient() Client {
	return &client{}
}

func (client) CreateUser(ctx context.Context, b users.Backends, phoneNumber, deviceIdentifier string) error {
	return users.CreateUser(ctx, b, phoneNumber, deviceIdentifier)
}

func (client) LookupUser(ctx context.Context, b users.Backends, phoneNumber string) (*public.User, error) {
	return db.LookupUser(ctx, b.Cassandra(), phoneNumber)
}

func (client) UpdateUser(ctx context.Context, b users.Backends, pn string, u *userspb.User) error {

	if pn != u.PhoneNumber {
		return errors.New("phone number did not match user update")
	}

	return db.UpdateUser(ctx, b.Cassandra(), &public.User{
		PhoneNumber:  u.PhoneNumber,
		EmailAddress: u.EmailAddress,
	})
}

func (client) DeleteUser(ctx context.Context, b users.Backends, pn string) error {
	return db.DeleteUser(ctx, b.Cassandra(), pn)
}
