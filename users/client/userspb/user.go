package userspb

import (
	"gitlab.com/shivam-909/authenticator/public"
)

func APIUser(u *public.User) *User {
	return &User{
		PhoneNumber:  u.PhoneNumber,
		EmailAddress: u.EmailAddress,
		Verified:     u.Verified,
		CreatedAt:    int32(u.CreatedAt.Unix()),
		UpdatedAt:    int32(u.CreatedAt.Unix()),
	}
}
