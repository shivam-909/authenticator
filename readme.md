# cassini authenticator

## 🔒 stateful, scalable and secure authentication for microservices with role based access control provided by ORY Keto.

authentication can get pretty messy at scale. jwts? sessions? galactus the all knowing user service provider aggregator? its too hard. so i decided to make it worse.

authenticator provides everything you need for a modern, secure(?) microservice architecture - or at least it will at some point, maybe.

the main two concerns with authentication are scalability and security.

### scaling

authenticator scales horizontally, with a datastore that also scales horizontally. cassandra is a distributed, nosql datastore, and the authenticator services can run many replicas. its designed with kubernetes in mind. 

### security

we believe security is achieved through layers. so we have a bunch of layers:

- The token:
    - Firstly, we're using JWTs. The first layer of "protection" is base64 encoding. This isn't really secure, but will prevent some two-bit internet pirates who saw a 8 minute hacking tutorial from bothering.
    - After you decode the token... you have a bunch of seemingly random bytes! The token is AES encrypted with a secure key thats rotated every 12 hours.
    - Lets say you somehow get our private key, and you're ready to start forging tokens. You still can't. You need the JWT signing key.
    - Lets say you somehow get *that*. You still can't make a valid token. Here's the structure of the token:
        ```
                type Token struct {
                    Eb string
                    Jti string
                }
        ```
    - Eb stands for entropy bucket. It's a cryptographically secure random string that we attach onto each token, and store. Notice that there's no sub, expiry or issuance time. This is stored alongside the entropy bucket to identify the user. This officially makes this implementation stateful.
    - There's also a JTI. When a token is successfully used, we store the jti they used. When we ingest a token for authentication, we check our JTI table and make sure its not one that has been used. If it has, we'll go ahead and see if that token's Eb matched a user, and delete all the tokens for that user, effectively completely logging them out.
    - So you'd need to get an Eb value we've stored, a JTI we haven't stored, our private key, our JWT signing key (both of which are rotated regularly and unique per user) and encode the keys - before that Eb value expires, which is 5 minutes.
    - This still doesn't deter a MITM attack, but we try and mitigate this risk using token reuse detection with the methods described above. We invalidate all tokens for a user if a single one is reused, and create new refresh tokens and access tokens on each refresh.

- Networking (writing coming soon...)

## ⚠️ **Current status: very, VERY unstable. Even this readme is garbage.**