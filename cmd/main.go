package main

import (
	"gitlab.com/shivam-909/authenticator"
	"gitlab.com/shivam-909/authenticator/authority/client/authoritypb/authoritypbconnect"
	"gitlab.com/shivam-909/authenticator/internal/server"
	"gitlab.com/shivam-909/authenticator/users/client/userspb/userspbconnect"
	"gitlab.com/shivam-909/authenticator/verify/client/verifypb/verifypbconnect"
)

func main() {
	s := server.Init()

	b := authenticator.MakeBackends()

	authorityServer := server.NewAuthorityServer(b)
	usersServer := server.NewUsersServer(b)
	verifyServer := server.NewVerifyServer(b)

	authorityPath, authorityHandler := authoritypbconnect.NewAuthorityServiceHandler(authorityServer)
	usersPath, usersHandler := userspbconnect.NewUsersServiceHandler(usersServer)
	verifyPath, verifyHandler := verifypbconnect.NewVerifyServiceHandler(verifyServer)

	s.RegisterRPC(authorityPath, authorityHandler)
	s.RegisterRPC(usersPath, usersHandler)
	s.RegisterRPC(verifyPath, verifyHandler)

	s.Serve(nil, "localhost:8080", "localhost:8081")
}
