package server

import (
	"context"

	"github.com/bufbuild/connect-go"
	"gitlab.com/shivam-909/authenticator/verify"
	verifycl "gitlab.com/shivam-909/authenticator/verify/client"
	"gitlab.com/shivam-909/authenticator/verify/client/verifypb"
)

type verifyServer struct {
	b  verify.Backends
	cl verifycl.Client
}

func NewVerifyServer(b verify.Backends) *verifyServer {
	cl := verifycl.NewLogicalClient()

	return &verifyServer{
		b:  b,
		cl: cl,
	}
}

func (s *verifyServer) RequestNewOTP(
	ctx context.Context,
	req *connect.Request[verifypb.Empty],
) (*connect.Response[verifypb.Empty], error) {

	return connect.NewResponse(&verifypb.Empty{}), nil
}

func (s *verifyServer) ResendEmailCode(
	ctx context.Context,
	req *connect.Request[verifypb.Empty],
) (*connect.Response[verifypb.Empty], error) {

	return connect.NewResponse(&verifypb.Empty{}), nil
}

func (s *verifyServer) VerifyPhoneNumber(
	ctx context.Context,
	req *connect.Request[verifypb.VerifyContactRequest],
) (*connect.Response[verifypb.Empty], error) {

	return connect.NewResponse(&verifypb.Empty{}), nil
}

func (s *verifyServer) VerifyEmailAddress(
	ctx context.Context,
	req *connect.Request[verifypb.VerifyContactRequest],
) (*connect.Response[verifypb.Empty], error) {

	return connect.NewResponse(&verifypb.Empty{}), nil
}
