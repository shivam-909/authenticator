package server

import (
	"context"

	"github.com/bufbuild/connect-go"
	"gitlab.com/shivam-909/authenticator/authority"
	authoritycl "gitlab.com/shivam-909/authenticator/authority/client"
	"gitlab.com/shivam-909/authenticator/authority/client/authoritypb"
	"gitlab.com/shivam-909/authenticator/users/client/userspb"
)

type authorityServer struct {
	b  authority.Backends
	cl authoritycl.Client
}

func NewAuthorityServer(b authority.Backends) *authorityServer {

	cl := authoritycl.NewLogicalClient()

	return &authorityServer{
		b:  b,
		cl: cl,
	}
}

func (s *authorityServer) Hello(
	ctx context.Context,
	req *connect.Request[authoritypb.Empty],
) (*connect.Response[authoritypb.HelloResponse], error) {
	return connect.NewResponse(&authoritypb.HelloResponse{
		Greeting: "Hey, this is Authority :)",
	}), nil
}

func (s *authorityServer) RegisterUser(
	ctx context.Context,
	req *connect.Request[authoritypb.RegisterUserRequest],
) (*connect.Response[authoritypb.RegisterUserResponse], error) {

	res, err := s.cl.RegisterUser(ctx, s.b, req.Msg.GetPhoneNumber(), req.Msg.GetDeviceIdentifier())
	if err != nil {
		return nil, err
	}
	return connect.NewResponse(&authoritypb.RegisterUserResponse{
		AccessToken:  res.Access,
		RefreshToken: res.Refresh,
	}), nil
}

func (s *authorityServer) Authenticate(
	ctx context.Context,
	req *connect.Request[authoritypb.AuthenticateRequest],
) (*connect.Response[authoritypb.AuthenticateResponse], error) {

	res, err := s.cl.Authenticate(ctx, s.b, req.Msg.AccessToken)
	if err != nil {
		return nil, err
	}

	return connect.NewResponse(&authoritypb.AuthenticateResponse{
		User: userspb.APIUser(res),
	}), nil
}

func (s *authorityServer) RefreshToken(
	ctx context.Context,
	req *connect.Request[authoritypb.RefreshTokenRequest],
) (*connect.Response[authoritypb.RefreshTokenResponse], error) {

	return nil, nil
}
