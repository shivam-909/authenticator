package server

import (
	"context"

	"github.com/bufbuild/connect-go"
	"gitlab.com/shivam-909/authenticator/users"
	userscl "gitlab.com/shivam-909/authenticator/users/client"
	"gitlab.com/shivam-909/authenticator/users/client/userspb"
)

type usersServer struct {
	b  users.Backends
	cl userscl.Client
}

func NewUsersServer(b users.Backends) *usersServer {

	cl := userscl.NewLogicalClient()

	return &usersServer{
		b:  b,
		cl: cl,
	}
}

func (s *usersServer) Lookup(
	ctx context.Context,
	req *connect.Request[userspb.IdRequest],
) (*connect.Response[userspb.User], error) {

	res, err := s.cl.LookupUser(ctx, s.b, req.Msg.GetPhoneNumber())
	if err != nil {
		return nil, err
	}
	return connect.NewResponse(userspb.APIUser(res)), nil
}

func (s *usersServer) UpdateUser(
	ctx context.Context,
	req *connect.Request[userspb.UpdateUserRequest],
) (*connect.Response[userspb.Empty], error) {

	return connect.NewResponse(&userspb.Empty{}),
		s.cl.UpdateUser(ctx, s.b, req.Msg.GetPhoneNumber(), req.Msg.GetUser())

}

func (s *usersServer) DeleteUser(
	ctx context.Context,
	req *connect.Request[userspb.IdRequest],
) (*connect.Response[userspb.Empty], error) {

	return connect.NewResponse(&userspb.Empty{}),
		s.cl.DeleteUser(ctx, s.b, req.Msg.GetPhoneNumber())
}
