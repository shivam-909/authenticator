package datastore

import (
	"fmt"
	"log"
	"strings"

	"github.com/gocql/gocql"
	"gitlab.com/shivam-909/authenticator/api"
)

func listHosts(h string) []string {
	hosts := []string{}
	for i := 9042; i < 9045; i++ {
		hosts = append(hosts, fmt.Sprintf("%v:%d", h, i))
	}

	return hosts
}

func Connect() *gocql.Session {
	cluster := gocql.NewCluster(listHosts("localhost")...)
	cluster.Keyspace = api.ServiceName
	session, err := cluster.CreateSession()
	if err != nil {
		panic(err)
	}

	fmt.Printf("✅ db connection established! connected to hosts: %v \n", cluster.Hosts)
	md, err := session.KeyspaceMetadata(cluster.Keyspace)
	if err != nil {
		log.Fatalf("⚠️ ‼️ keyspace %v was not found.", cluster.Keyspace)
	}

	fmt.Printf("🤗 cassandra session on keyspace %v is running. \n", strings.ToUpper(cluster.Keyspace))

	fmt.Printf("ℹ️ tables on this keyspace: %v \n", len(md.Tables))
	for _, v := range md.Tables {
		fmt.Println("⎍ ||" + strings.ToUpper(v.Name) + "||")
	}

	return session
}
