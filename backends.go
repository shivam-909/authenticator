package authenticator

import (
	"github.com/gocql/gocql"
	"gitlab.com/shivam-909/authenticator/internal/datastore"
	userscl "gitlab.com/shivam-909/authenticator/users/client"
)

type Backends interface {
	Cassandra() *gocql.Session
	UsersClient() userscl.Client
}

type backends struct {
	cassandra_client *gocql.Session
	users_client     userscl.Client
}

func MakeBackends() Backends {
	b := &backends{}

	b.cassandra_client = datastore.Connect()
	b.users_client = userscl.NewLogicalClient()

	return b
}

func (b *backends) Cassandra() *gocql.Session {
	return b.cassandra_client
}

func (b *backends) UsersClient() userscl.Client {
	return b.users_client
}
