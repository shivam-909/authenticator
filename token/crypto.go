package token

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"io"
	"math/big"
	"strings"

	"gitlab.com/shivam-909/heck"
)

func GenerateRandomString(n int) (string, error) {
	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-"
	ret := make([]byte, n)
	for i := 0; i < n; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(letters))))
		if err != nil {
			return "", err
		}
		ret[i] = letters[num.Int64()]
	}

	return string(ret), nil
}

func AESEncrypt(plaintext string, key []byte) ([]byte, error) {

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, heck.Wrap("failed to create cipher", err)
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, heck.Wrap("failed to create new gcm", err)
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, heck.Wrap("failed to read random bytes into nonce", err)
	}

	return gcm.Seal(nonce, nonce, []byte(plaintext), nil), nil
}

func AESDecrypt(encrypted []byte, key []byte) (string, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return "", heck.Wrap("failed to create cipher", err)
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return "", heck.Wrap("failed to create new gcm", err)
	}

	nonceSize := gcm.NonceSize()
	if len(encrypted) < nonceSize {
		return "", errors.New("length of encrypted data did not match nonce size")
	}

	nonce, ciphertext := encrypted[:nonceSize], encrypted[nonceSize:]

	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return "", heck.Wrap("failed to open gcm and decrypt string", err)
	}

	return string(plaintext), nil

}

func B64AESEncrypt(plaintext string, key []byte) (string, error) {
	enc, err := AESEncrypt(plaintext, key)
	if err != nil {
		return "", heck.Wrap("failed to encrypt plaintext", err)
	}

	str := base64.StdEncoding.EncodeToString(enc)
	return str, nil
}

func B64AESDecrypt(encoded string, key []byte) (string, error) {
	b, err := base64.StdEncoding.DecodeString(encoded)
	if err != nil {
		return "", heck.Wrap("failed to b64 decode string", err)
	}

	return AESDecrypt(b, key)
}

func TruncateTo32Bytes(s string) []byte {
	spl := strings.Split(s, "")
	ret := []string{}

	if len(spl) > 32 {
		for i := 0; i < 32; i++ {
			ret = append(ret, spl[i])
		}
	} else {
		spl = append(spl, repeatingStringOfLength("f", 32-len(spl))...)
		ret = append(ret, spl...)
	}

	return []byte(strings.Join(ret, ""))
}

func repeatingStringOfLength(char string, len int) []string {
	ret := []string{}
	for i := 0; i < len; i++ {
		ret = append(ret, char)
	}
	return ret
}
