package db

import (
	"context"
	"time"

	"github.com/gocql/gocql"
	"gitlab.com/shivam-909/authenticator/public"
	"gitlab.com/shivam-909/heck"
)

const (
	allFields   = " eb, expiry, issued, sub "
	insertToken = "INSERT INTO authenticator.tokens (eb, expiry, issued, sub) VALUES (?, ?, ?, ?);"
	lookupToken = "SELECT" + allFields + "FROM authenticator.tokens WHERE eb = ?;"
)

func InsertToken(ctx context.Context, dbc *gocql.Session, expiry, iss time.Time, sub, eb string) error {
	err := dbc.Query(insertToken, eb, expiry, iss, sub).Exec()
	if err != nil {
		return heck.Wrap("failed to exec query", err)
	}
	return nil
}

func LookupToken(ctx context.Context, dbc *gocql.Session, eb string) (*public.Token, error) {
	t := &public.Token{}

	err := dbc.Query(lookupToken, eb).Scan(&t.Eb, &t.Expiry, &t.Issued, &t.Subject)
	if err != nil {
		return nil, heck.Wrap("failed to scan token", err)
	}

	return t, nil
}
