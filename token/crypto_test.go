package token

import (
	"encoding/base64"
	"testing"

	"gitlab.com/shivam-909/heck"
)

func TestEncryption(t *testing.T) {
	scenarios := []struct {
		key  []byte
		data string
	}{
		{
			key:  []byte("passphrasewhichneedstobe32bytes!"),
			data: "thisis",
		},
	}

	for _, sc := range scenarios {
		encrypted, err := B64AESEncrypt(sc.data, sc.key)
		heck.RequireNil(t, err)

		t.Logf("encrypted data: %v \n", encrypted)

		dc, err := base64.StdEncoding.DecodeString(encrypted)
		heck.RequireNil(t, err)

		t.Logf("encrypted data, decoded: %v", string(dc))

		decrypted, err := B64AESDecrypt(encrypted, sc.key)
		heck.RequireNil(t, err)

		if decrypted != sc.data {
			t.Fatalf("decrypted and input did not match: \n decrypted data: %v \n input data: %v", decrypted, sc.data)
		}
	}
}
