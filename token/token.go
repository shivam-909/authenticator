package token

import (
	"context"
	"errors"
	"time"

	"github.com/gocql/gocql"
	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/shivam-909/authenticator/public"
	"gitlab.com/shivam-909/authenticator/token/db"
	"gitlab.com/shivam-909/heck"
)

var (
	timeToExpire = (5 * time.Minute)
	ebLength     = 8

	signingKey    []byte = []byte("{{SECURE_SIGNING_KEY}}")
	encryptionKey        = "{{SECURE_ENCRYPTION_KEY}}"
)

type Pair struct {
	Access  string `json:"access_token"`
	Refresh string `json:"refresh_token"`
}

type Claims struct {
	Eb  string `json:"eb"`
	Jti string `json:"jti"`
	jwt.StandardClaims
}

// NewToken creates a token pair for the subject provided and persists the tokens to the tokens table.
func NewToken(ctx context.Context, b Backends, sub string) (*Pair, error) {

	// Check that the sub actually matches a user
	_, err := b.UsersClient().LookupUser(ctx, b, sub)
	if err != nil {
		if err == gocql.ErrNotFound {
			return nil, errors.New("user does not exist")
		}
		return nil, err
	}

	// Set timestamps
	now := time.Now()
	expiry := time.Now().Add(timeToExpire)

	// Create claims
	accessClaims, err := newClaims()
	if err != nil {
		return nil, heck.Wrap("failed to create new access claims", err)
	}

	refreshClaims, err := newClaims()
	if err != nil {
		return nil, heck.Wrap("failed to create new refresh claims", err)

	}

	// Create tokens
	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, accessClaims)
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshClaims)

	// Sign tokens and get strings
	accessString, err := accessToken.SignedString(signingKey)
	if err != nil {
		return nil, heck.Wrap("failed to create new access string", err)
	}

	refreshString, err := refreshToken.SignedString(signingKey)
	if err != nil {
		return nil, heck.Wrap("failed to create new refresh string", err)

	}

	// TODO(shivam): Add key prefix fetching from Vault

	// Encrypt and encode token strings
	asenc, err := B64AESEncrypt(accessString, TruncateTo32Bytes(encryptionKey))
	if err != nil {
		return nil, heck.Wrap("failed to encrypt and encode access string", err)
	}

	rsenc, err := B64AESEncrypt(refreshString, TruncateTo32Bytes(encryptionKey))
	if err != nil {
		return nil, heck.Wrap("failed to encrypt and encode refresh string", err)
	}

	// Insert tokens into datastore
	err = db.InsertToken(ctx, b.Cassandra(), expiry, now, sub, accessClaims.Eb)
	if err != nil {
		return nil, heck.Wrap("failed to insert access token", err)
	}

	err = db.InsertToken(ctx, b.Cassandra(), expiry, now, sub, refreshClaims.Eb)
	if err != nil {
		return nil, heck.Wrap("failed to insert refresh token", err)

	}

	return &Pair{
		Access:  asenc,
		Refresh: rsenc,
	}, nil
}

func ClaimsFromTokenString(ctx context.Context, b Backends, accessToken string) (*Claims, error) {
	decryptedJwt, err := B64AESDecrypt(accessToken, TruncateTo32Bytes(encryptionKey))
	if err != nil {
		return nil, heck.Wrap("failed to decrypt base64 encoded access token", err)
	}

	claims := &Claims{}

	tkn, err := jwt.ParseWithClaims(decryptedJwt, claims, func(token *jwt.Token) (interface{}, error) {
		return signingKey, nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return nil, public.ErrInvalidSignature
		}
		return nil, heck.Wrap("failed to parse jwt", err)
	}

	if !tkn.Valid {
		return nil, public.ErrUnauthorisedJwt
	}

	return claims, nil
}

func newClaims() (*Claims, error) {

	eb, err := GenerateRandomString(ebLength)
	if err != nil {
		return nil, heck.Wrap("failed to generate random bytes", err)
	}

	jti, err := gocql.RandomUUID()
	if err != nil {
		return nil, heck.Wrap("failed to generate random uuid", err)
	}

	claims := &Claims{
		Eb:  eb,
		Jti: jti.String(),
	}
	return claims, nil
}
