package token

import (
	"github.com/gocql/gocql"
	userscl "gitlab.com/shivam-909/authenticator/users/client"
)

type Backends interface {
	Cassandra() *gocql.Session
	UsersClient() userscl.Client
}
