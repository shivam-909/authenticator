package public

import (
	"net/mail"
	"strings"
	"time"
)

type CountryCode string

const (
	CC_UK CountryCode = "UK"
)

var (
	phoneNumberValidationByCountry = map[CountryCode]func(string) bool{
		CC_UK: validPhoneNumberUK,
	}
)

type User struct {
	PhoneNumber      string
	EmailAddress     string
	Verified         bool
	DeviceIdentifier string
	PinHash          string
	CreatedAt        time.Time
	UpdatedAt        time.Time
}

func (u *User) Valid() bool {
	return ValidEmailAddress(u.EmailAddress) && ValidPhoneNumber(u.PhoneNumber)
}

func ValidEmailAddress(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func ValidPhoneNumber(pn string) bool {
	var foundValid bool
	for _, fn := range phoneNumberValidationByCountry {
		if fn(pn) {
			foundValid = true
			break
		}
	}
	return foundValid
}

func validPhoneNumberUK(pn string) bool {
	spl := strings.Split(pn, "")
	if len(spl) != 13 {
		return false
	}

	if strings.Join(spl[:3], "") != "+44" {
		return false
	}

	return true
}
