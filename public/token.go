package public

import "time"

type Token struct {
	Eb      string
	Expiry  time.Time
	Issued  time.Time
	Subject string
}
