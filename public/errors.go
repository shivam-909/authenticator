package public

import "errors"

var (
	ErrInvalidPhoneNumber = errors.New("invalid phone number")
	ErrUnauthorisedJwt    = errors.New("jwt is invalid")
	ErrInvalidSignature   = errors.New("invalid signature")

	ErrJtiUsed        = errors.New("jti reused")
	ErrEbDoesNotExist = errors.New("eb does not exist")
)
