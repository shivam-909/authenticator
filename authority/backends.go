package authority

import (
	userscl "gitlab.com/shivam-909/authenticator/users/client"

	"github.com/gocql/gocql"
)

type Backends interface {
	Cassandra() *gocql.Session
	UsersClient() userscl.Client
}
