package authority

import (
	"context"

	"gitlab.com/shivam-909/authenticator/authority/db"
	"gitlab.com/shivam-909/authenticator/public"
	"gitlab.com/shivam-909/authenticator/token"
	token_db "gitlab.com/shivam-909/authenticator/token/db"
	"gitlab.com/shivam-909/heck"
)

func RegisterUser(ctx context.Context, b Backends, phoneNumber, deviceIdentifer string) (*token.Pair, error) {

	if !public.ValidPhoneNumber(phoneNumber) {
		return nil, public.ErrInvalidPhoneNumber
	}

	err := b.UsersClient().CreateUser(ctx, b, phoneNumber, deviceIdentifer)
	if err != nil {
		return nil, heck.Wrap("failed to create new user", err)
	}

	pair, err := token.NewToken(ctx, b, phoneNumber)
	if err != nil {
		return nil, heck.Wrap("failed to generate new token", err)
	}

	return &token.Pair{
		Access:  pair.Access,
		Refresh: pair.Refresh,
	}, nil
}

func AuthenticateUser(ctx context.Context, b Backends, accessToken string) (*public.User, error) {

	claims, err := token.ClaimsFromTokenString(ctx, b, accessToken)
	if err != nil {
		return nil, heck.Wrap("failed to ingest token", err)
	}

	count, err := db.LookupJti(ctx, b.Cassandra(), claims.Jti)
	if err != nil {
		return nil, heck.Wrap("failed to lookup jti", err)
	}

	if count != 0 {
		// TODO(shivam): Alarms should go off
		return nil, public.ErrJtiUsed
	}

	token, err := token_db.LookupToken(ctx, b.Cassandra(), claims.Eb)
	if err != nil {
		return nil, heck.Wrap("failed to lookup token", err)
	}

	if token.Eb == "" {
		return nil, public.ErrEbDoesNotExist
	}

	user, err := b.UsersClient().LookupUser(ctx, b, token.Subject)
	if err != nil {
		return nil, heck.Wrap("could not lookup user", err)
	}

	return user, nil
}
