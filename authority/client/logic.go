package client

import (
	"context"

	"gitlab.com/shivam-909/authenticator/authority"
	"gitlab.com/shivam-909/authenticator/public"
	"gitlab.com/shivam-909/authenticator/token"
)

// logic contains all business logic methods for this package to expose.

type Client interface {
	RegisterUser(ctx context.Context, b authority.Backends, phoneNumber, deviceIdentifer string) (*token.Pair, error)
	Authenticate(ctx context.Context, b authority.Backends, accessToken string) (*public.User, error)
}

type client struct{}

func NewLogicalClient() Client {
	return &client{}
}

func (client) RegisterUser(ctx context.Context, b authority.Backends, phoneNumber, deviceIdentifier string) (*token.Pair, error) {
	return authority.RegisterUser(ctx, b, phoneNumber, deviceIdentifier)
}

func (client) Authenticate(ctx context.Context, b authority.Backends, accessToken string) (*public.User, error) {
	return authority.AuthenticateUser(ctx, b, accessToken)
}
