package db

import (
	"context"

	"github.com/gocql/gocql"
	"gitlab.com/shivam-909/heck"
)

const (
	lookupJti = "SELECT COUNT(*) FROM jtis WHERE jti = ? LIMIT 1;"
)

func LookupJti(ctx context.Context, dbc *gocql.Session, jti string) (int, error) {
	id, err := gocql.ParseUUID(jti)
	if err != nil {
		return 0, heck.Wrap("failed to parse uuid", err)
	}

	var count int
	err = dbc.Query(lookupJti, id).Scan(&count)
	if err != nil {
		if heck.IsAny(err, gocql.ErrNotFound) {
			return 0, err
		}
		return 0, heck.Wrap("failed to scan query", err)
	}

	return count, nil
}
